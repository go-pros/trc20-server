package handlers

import (
	"context"

	pb "gitee.com/go-pros/trc20-server"
)

// NewService returns a naïve, stateless implementation of Service.
func NewService() pb.UsersServer {
	return usersService{}
}

type usersService struct{}

func (s usersService) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	var resp pb.HelloReply
	return &resp, nil
}
