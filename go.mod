module gitee.com/go-pros/trc20-server

go 1.15

require (
	github.com/go-kit/kit v0.10.0
	github.com/gogo/protobuf v1.3.2
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1
	google.golang.org/grpc v1.37.0
)
