package main

import (
	"flag"
	"gitee.com/go-pros/trc20-server/users-service/svc/server"
)

func main()  {

		// Update addresses if they have been overwritten by flags
		flag.Parse()

		server.Run(server.DefaultConfig)

}
